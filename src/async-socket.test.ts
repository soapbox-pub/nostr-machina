import { AsyncSocket } from './async-socket.ts';
import { waitForAbort } from './async-utils.ts';
import { assertEquals } from './deps-test.ts';
import { testServer } from './test-helpers.ts';

Deno.test('messages', async () => {
  const { url, close } = testServer();

  const socket = new WebSocket(url);
  const controller = new AbortController();
  const messages = AsyncSocket.messages(socket, controller.signal);
  const results: string[] = [];

  (async () => {
    for await (const { data } of messages) {
      results.push(data);

      if (results.length > 1) {
        controller.abort();
      }
    }
  })();

  socket.dispatchEvent(
    new MessageEvent('message', {
      data: 'foo',
    }),
  );

  socket.dispatchEvent(
    new MessageEvent('message', {
      data: 'bar',
    }),
  );

  await waitForAbort(controller.signal);
  await close();

  assertEquals(results, ['foo', 'bar']);
});

Deno.test('send', async () => {
  const results: string[] = [];

  const { url, close } = testServer((_socket, e) => {
    results.push(e.data);
  });

  const socket = new WebSocket(url);

  await AsyncSocket.send(socket, 'foo');
  await AsyncSocket.send(socket, 'bar');
  await close();

  assertEquals(results, ['foo', 'bar']);
});

Deno.test('waitForOpen', async () => {
  const { url, close } = testServer();
  const socket = new WebSocket(url);
  assertEquals(socket.readyState, WebSocket.CONNECTING);
  await AsyncSocket.waitForOpen(socket);
  assertEquals(socket.readyState, WebSocket.OPEN);
  await close();
});
