import { AsyncSocket } from './async-socket.ts';
import { sleep, waitForAbort } from './async-utils.ts';
import { assertEquals, assertNotEquals } from './deps-test.ts';
import { type Event } from './deps.ts';
import { NiceRelay } from './nice-relay.ts';
import { testServer } from './test-helpers.ts';

const testEvents = {
  note: {
    kind: 1,
    content: 'I\'m vegan btw',
    tags: [
      [
        'proxy',
        'https://gleasonator.com/objects/8f6fac53-4f66-4c6e-ac7d-92e5e78c3e79',
        'activitypub',
      ],
    ],
    pubkey: '79c2cae114ea28a981e7559b4fe7854a473521a8d22a66bbab9fa248eb820ff6',
    created_at: 1691091365,
    id: '55920b758b9c7b17854b6e3d44e6a02a83d1cb49e1227e75a30426dea94d4cb2',
    sig:
      'a72f12c08f18e85d98fb92ae89e2fe63e48b8864c5e10fbdd5335f3c9f936397a6b0a7350efe251f8168b1601d7012d4a6d0ee6eec958067cf22a14f5a5ea579',
  },
};

Deno.test('send', async () => {
  const server = testServer((socket, { data }) => {
    const [_, event] = JSON.parse(data);
    socket.send(JSON.stringify(['OK', event.id, true, '']));
  });

  const relay = new NiceRelay(server.url);
  await server.waitForOpen;

  const event = testEvents.note;
  const result = await relay.send(['EVENT', event], { timeout: 1000 });

  await relay.close();
  await server.close();

  assertEquals(result, ['OK', event.id, true, '']);
});

Deno.test('req', async () => {
  const server = testServer((socket, { data }) => {
    const [_, id, _filters] = JSON.parse(data);
    socket.send(JSON.stringify(['EVENT', id, testEvents.note]));
    socket.send(JSON.stringify(['EOSE', id]));
  });

  const relay = new NiceRelay(server.url);
  await server.waitForOpen;

  const sub = relay.req([
    { kinds: [1], authors: ['79c2cae114ea28a981e7559b4fe7854a473521a8d22a66bbab9fa248eb820ff6'] },
  ]);

  waitForAbort(sub.eoseSignal).then(() => sub.close());

  const results: Event<1>[] = [];

  for await (const event of sub) {
    results.push(event);
  }

  await relay.close();
  await server.close();

  assertEquals(results.length, 1);
  assertEquals(results[0].id, testEvents.note.id);
});

Deno.test('resumes REQs after disconnect', async () => {
  const msgs: string[] = [];
  const server = testServer((socket, { data }) => {
    msgs.push(data);
    const [_, id, _filters] = JSON.parse(data);
    socket.send(JSON.stringify(['EVENT', id, testEvents.note]));
    socket.send(JSON.stringify(['EOSE', id]));
  });

  const relay = new NiceRelay(server.url, { backoff: () => 0 });
  await server.waitForOpen;
  assertEquals(relay.socket.readyState, WebSocket.OPEN);

  const sub = relay.req([
    { kinds: [1], authors: ['79c2cae114ea28a981e7559b4fe7854a473521a8d22a66bbab9fa248eb820ff6'] },
  ]);

  let results: Event<1>[] = [];

  // Break out after the first event, but keep the sub open.
  for await (const event of sub) {
    results.push(event);
    break;
  }

  assertEquals(results.length, 1);
  assertEquals(results[0].id, testEvents.note.id);

  // Close the server, which should trigger a disconnect.
  const oldSocket = relay.socket;
  await server.disconnect();
  assertEquals(oldSocket.readyState, WebSocket.CLOSED);
  assertNotEquals(relay.socket, oldSocket);

  // Wait for relay to reconnect.
  await AsyncSocket.waitForOpen(relay.socket);
  assertEquals(relay.socket.readyState, WebSocket.OPEN);

  // Wait for the REQ to be resent.
  while (msgs.length < 2) {
    await sleep(10);
  }

  results = [];

  // Abort on EOSE this time.
  waitForAbort(sub.eoseSignal).then(() => sub.close());

  for await (const event of sub) {
    results.push(event);
  }

  await relay.close();
  await server.close();

  assertEquals(results.length, 1);
  assertEquals(results[0].id, testEvents.note.id);

  // Two REQs and one CLOSE.
  assertEquals(msgs.length, 3);
});
