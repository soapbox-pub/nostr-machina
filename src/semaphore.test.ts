import { sleep } from './async-utils.ts';
import { Semaphore, type SemaphoreAcquireOpts } from './semaphore.ts';
import { assertEquals } from './deps-test.ts';

async function limitedAsyncFunction(
  semaphore: Semaphore,
  func: () => unknown,
  opts?: SemaphoreAcquireOpts,
) {
  const controller = new AbortController();
  const { signal } = controller;
  await semaphore.acquire({ signal, ...opts });
  try {
    return await func();
  } finally {
    controller.abort();
  }
}

Deno.test('should limit concurrent access', async () => {
  const semaphore = new Semaphore(2);
  let concurrent = 0;
  let maxConcurrent = 0;

  const testFunction = async () => {
    concurrent++;
    maxConcurrent = Math.max(maxConcurrent, concurrent);
    await sleep(100);
    concurrent--;
  };

  await Promise.all([
    limitedAsyncFunction(semaphore, testFunction),
    limitedAsyncFunction(semaphore, testFunction),
    limitedAsyncFunction(semaphore, testFunction),
  ]);

  assertEquals(maxConcurrent, 2);
});

Deno.test('should respect priority', async () => {
  const semaphore = new Semaphore(1);
  const executionOrder: number[] = [];

  const testFunction = (order: number) => async () => {
    await sleep(50);
    executionOrder.push(order);
  };

  await Promise.all([
    limitedAsyncFunction(semaphore, testFunction(3), { priority: 0 }),
    limitedAsyncFunction(semaphore, testFunction(1), { priority: 99 }),
    limitedAsyncFunction(semaphore, testFunction(2), { priority: 50 }),
  ]);

  assertEquals(executionOrder, [3, 1, 2]);
});

Deno.test('should handle abort signal', async () => {
  const semaphore = new Semaphore(1);
  let wasAborted = false;

  const controller = new AbortController();
  const { signal } = controller;

  const longRunningTask = async () => {
    await sleep(100);
  };

  // Start a long-running task to occupy the semaphore.
  limitedAsyncFunction(semaphore, longRunningTask);

  // This task will be queued but then aborted.
  limitedAsyncFunction(semaphore, async () => {}, { signal })
    .catch((error) => {
      if (error.name === 'AbortError') {
        wasAborted = true;
      }
    });

  // Abort the second task.
  controller.abort();

  // Wait a bit to ensure the abort has been processed.
  await sleep(10);

  // Ensure the aborted task was correctly handled.
  assertEquals(wasAborted, true);

  // Also ensure that the semaphore is not blocked by the aborted task.
  let isBlocked = false;
  await limitedAsyncFunction(semaphore, () => {
    isBlocked = true;
  }).catch(() => {
    isBlocked = false;
  });

  assertEquals(isBlocked, true);
});
