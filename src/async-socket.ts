import { Machina } from './machina.ts';

/** Get WebSocket events as an async iterator. */
async function* messages(socket: WebSocket, signal: AbortSignal): AsyncGenerator<MessageEvent> {
  const machina = new Machina<MessageEvent>();

  socket.addEventListener('message', handleMessage);
  socket.addEventListener('close', handleClose);
  signal.addEventListener('abort', handleClose);

  function handleMessage(ev: MessageEvent): void {
    machina.push(ev);
  }

  function handleClose(): void {
    machina.close();
  }

  for await (const msg of machina.stream()) {
    yield msg;
  }

  socket.removeEventListener('message', handleMessage);
  socket.removeEventListener('close', handleClose);
  signal.removeEventListener('abort', handleClose);
}

/** Send a message to a WebSocket, waiting for the socket to open if necessary. */
async function send(socket: WebSocket, data: string, signal?: AbortSignal): Promise<void> {
  await waitForOpen(socket, signal);
  socket.send(data);
}

/** Wait for a WebSocket to open. */
async function waitForOpen(socket: WebSocket, signal?: AbortSignal): Promise<void> {
  if (signal?.aborted) throw new Error('Signal aborted.');

  switch (socket.readyState) {
    case WebSocket.OPEN:
      return;
    case WebSocket.CONNECTING:
      return await new Promise<void>((resolve, reject) => {
        function onOpen() {
          cleanup();
          resolve();
        }
        function onClose() {
          cleanup();
          reject(new Error('WebSocket closed before opening.'));
        }
        function onAbort() {
          cleanup();
          reject(new Error('Signal aborted.'));
        }
        function cleanup() {
          socket.removeEventListener('open', onOpen);
          socket.removeEventListener('close', onClose);
          signal?.removeEventListener('abort', onAbort);
        }
        socket.addEventListener('open', onOpen, { once: true });
        socket.addEventListener('close', onClose, { once: true });
        signal?.addEventListener('abort', onAbort, { once: true });
      });
    case WebSocket.CLOSING:
    case WebSocket.CLOSED:
      throw new Error('WebSocket is closing or closed.');
  }
}

/** Close the socket, and then wait for it to be closed. */
async function close(socket: WebSocket, code?: number, reason?: string): Promise<void> {
  switch (socket.readyState) {
    case WebSocket.CLOSED:
      return;
    case WebSocket.OPEN:
    case WebSocket.CONNECTING:
    case WebSocket.CLOSING:
      return await new Promise<void>((resolve) => {
        socket.addEventListener('close', () => resolve(), { once: true });
        socket.close(code, reason);
      });
  }
}

/** Functions that make WebSockets more pleasant to use with async code. */
const AsyncSocket = {
  messages,
  send,
  waitForOpen,
  close,
};

export { AsyncSocket };
