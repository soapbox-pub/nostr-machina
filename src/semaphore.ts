type ReleaseFn = () => void;

interface SemaphoreQueueItem {
  priority: number;
  resolve: () => void;
}

interface SemaphoreAcquireOpts {
  signal?: AbortSignal;
  priority?: number;
}

class Semaphore {
  #queue: SemaphoreQueueItem[] = [];
  #count: number;

  constructor(count: number) {
    this.#count = count;
  }

  async acquire(opts: SemaphoreAcquireOpts): Promise<void> {
    const { priority = 0, signal } = opts;

    if (signal?.aborted) {
      throw new DOMException('The operation was aborted.', 'AbortError');
    }

    if (this.#count > 0) {
      this.#count--;
      signal?.addEventListener('abort', this.#release, { once: true });
      return;
    }

    return await new Promise((resolve, reject) => {
      const queueItem = { priority, resolve };
      this.#enqueue(queueItem);

      signal?.addEventListener('abort', () => {
        const isRemoved = this.#removeFromQueue(queueItem);
        if (isRemoved) {
          reject(new DOMException('The operation was aborted.', 'AbortError'));
        } else {
          this.#release();
        }
      }, { once: true });
    });
  }

  #release: ReleaseFn = () => {
    const next = this.#dequeue();
    if (next) {
      next.resolve();
    } else {
      this.#count++;
    }
  };

  #enqueue(queueItem: SemaphoreQueueItem): void {
    const index = this.#queue.findIndex((item) => item.priority < queueItem.priority);
    if (index === -1) {
      this.#queue.push(queueItem);
    } else {
      this.#queue.splice(index, 0, queueItem);
    }
  }

  #dequeue(): SemaphoreQueueItem | undefined {
    return this.#queue.shift();
  }

  #removeFromQueue(queueItem: SemaphoreQueueItem): boolean {
    const index = this.#queue.indexOf(queueItem);
    const exists = index !== -1;
    if (exists) {
      this.#queue.splice(index, 1);
    }
    return exists;
  }
}

export { Semaphore, type SemaphoreAcquireOpts };
