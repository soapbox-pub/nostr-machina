import { exponential, fibonacci } from './backoff.ts';
import { assertEquals } from './deps-test.ts';

const random = () => 0.5;

Deno.test('exponential', () => {
  assertEquals(exponential(0, 1000, random), 500);
  assertEquals(exponential(1, 1000, random), 1000);
  assertEquals(exponential(2, 1000, random), 2000);
  assertEquals(exponential(3, 1000, random), 4000);
});

Deno.test('fibonacci', () => {
  assertEquals(fibonacci(0, 1000, random), 1000);
  assertEquals(fibonacci(1, 1000, random), 1000);
  assertEquals(fibonacci(2, 1000, random), 500);
  assertEquals(fibonacci(3, 1000, random), 1000);
  assertEquals(fibonacci(4, 1000, random), 1500);
  assertEquals(fibonacci(5, 1000, random), 2500);
});
