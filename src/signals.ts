class Signals {
  /** Produces a signal that aborts when all signals abort. */
  static all(signals: AbortSignal[]): AbortSignal {
    if (signals.every((signal) => signal.aborted)) {
      return AbortSignal.abort();
    }

    const controller = new AbortController();

    const handleAbort: EventListener = (e) => {
      e.currentTarget!.removeEventListener('abort', handleAbort);

      if (signals.every((signal) => signal.aborted)) {
        controller.abort();
      }
    };

    signals.forEach((signal) => {
      signal.addEventListener('abort', handleAbort, { once: true });
    });

    return controller.signal;
  }

  /** Produces a signal that aborts when any signal aborts. */
  static any(signals: AbortSignal[]): AbortSignal {
    if (signals.some((signal) => signal.aborted)) {
      return AbortSignal.abort();
    }

    const controller = new AbortController();

    const handleAbort = () => {
      controller.abort();
      signals.forEach((signal) => {
        signal.removeEventListener('abort', handleAbort);
      });
    };

    signals.forEach((signal) => {
      signal.addEventListener('abort', handleAbort, { once: true });
    });

    return controller.signal;
  }
}

export { Signals };
