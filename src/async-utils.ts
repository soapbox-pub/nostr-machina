/** Resolves the promise when `ms` have passed. */
function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/** Resolves the promise when the signal is aborted. */
async function waitForAbort(signal: AbortSignal) {
  if (!signal.aborted) {
    await new Promise((resolve) => signal.addEventListener('abort', resolve, { once: true }));
  }
}

export { sleep, waitForAbort };
