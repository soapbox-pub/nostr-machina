import { assertEquals } from './deps-test.ts';
import { Relay } from './relay.ts';
import { testServer } from './test-helpers.ts';

Deno.test('Relay', async () => {
  const results: string[] = [];

  const { url, close } = testServer((_socket, e) => {
    results.push(e.data);
  });

  const relay = new Relay(new WebSocket(url));

  await relay.send(['EVENT', 'foo', 'bar']);
  await relay.send(['EOSE', 'foo']);
  await close();

  assertEquals(results[0], JSON.stringify(['EVENT', 'foo', 'bar']));
  assertEquals(results[1], JSON.stringify(['EOSE', 'foo']));
});
