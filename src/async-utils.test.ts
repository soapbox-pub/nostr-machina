import { sleep, waitForAbort } from './async-utils.ts';
import { assert, assertGreater, assertLess } from './deps-test.ts';

Deno.test('sleep', async () => {
  const start = Date.now();
  await sleep(100);
  const end = Date.now();
  const diff = end - start;
  assertGreater(diff, 90);
  assertLess(diff, 110);
});

Deno.test('waitForAbort', async () => {
  const controller = new AbortController();
  assert(!controller.signal.aborted);
  const promise = waitForAbort(controller.signal);
  controller.abort();
  await promise;
  assert(controller.signal.aborted);
});

Deno.test('waitForAbort with an already aborted signal', async () => {
  const controller = new AbortController();
  controller.abort();
  await waitForAbort(controller.signal);
  assert(controller.signal.aborted);
});
