import { AsyncSocket } from './async-socket.ts';
import { assert, assertEquals, assertNotEquals } from './deps-test.ts';
import { reanimate } from './reanimate.ts';
import { testServer } from './test-helpers.ts';

Deno.test('reanimate', async () => {
  const server = testServer();
  const socket = new WebSocket(server.url);

  // When the socket disconnects, we can get the next socket by iterating.
  const promise = new Promise<WebSocket>((resolve) => {
    reanimate(socket, resolve);
  });

  // Disconnect the server, which should cause the socket to close.
  await server.waitForOpen;
  await server.disconnect();

  // The socket has reanimated. Ensure this is a new socket now.
  const newSocket = await promise;
  assert(newSocket);
  await AsyncSocket.waitForOpen(newSocket);
  assertNotEquals(newSocket, socket);

  // It's open and connected.
  assertEquals(newSocket.readyState, WebSocket.OPEN);
  assertEquals(newSocket.url, server.url);

  // Cleanup.
  newSocket.close();
  await server.close();
});
