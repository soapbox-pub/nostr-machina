import { assertEquals } from './deps-test.ts';
import { Signals } from './signals.ts';

Deno.test('Signals.any', () => {
  const controller1 = new AbortController();
  const controller2 = new AbortController();
  const controller3 = new AbortController();

  const signal1 = controller1.signal;
  const signal2 = controller2.signal;
  const signal3 = controller3.signal;

  const joinedSignal = Signals.any([signal1, signal2]);
  const joinedSignal2 = Signals.any([signal1, signal3]);

  assertEquals(joinedSignal.aborted, false);
  assertEquals(joinedSignal2.aborted, false);

  controller2.abort();
  assertEquals(joinedSignal.aborted, true);
  assertEquals(joinedSignal2.aborted, false);

  controller1.abort();
  assertEquals(joinedSignal.aborted, true);
  assertEquals(joinedSignal2.aborted, true);
});

Deno.test('Signals.all', () => {
  const controller1 = new AbortController();
  const controller2 = new AbortController();
  const controller3 = new AbortController();

  const signal1 = controller1.signal;
  const signal2 = controller2.signal;
  const signal3 = controller3.signal;

  const joinedSignal = Signals.all([signal1, signal2]);
  const joinedSignal2 = Signals.all([signal1, signal3]);

  assertEquals(joinedSignal.aborted, false);
  assertEquals(joinedSignal2.aborted, false);

  controller2.abort();
  assertEquals(joinedSignal.aborted, false);
  assertEquals(joinedSignal2.aborted, false);

  controller1.abort();
  assertEquals(joinedSignal.aborted, true);
  assertEquals(joinedSignal2.aborted, false);

  controller3.abort();
  assertEquals(joinedSignal.aborted, true);
  assertEquals(joinedSignal2.aborted, true);
});
