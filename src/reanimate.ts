import { type Backoff, exponential } from './backoff.ts';

/** Options for the `reanimate` function. */
interface ReanimateOpts {
  /** Function to compute the timeout between reconnections. */
  backoff?: Backoff;
  /** Initial delay for reconnections. */
  delay?: number;
}

/**
 * Keep the WebSocket connected. When closed, a new socket is created.
 *
 * By default, it uses exponential backoff with a 1 second delay.
 * You can change this behavior by passing in a `backoff` function and `delay` value.
 *
 * @example
 * ```ts
 * const socket = new WebSocket('wss://example.com');
 *
 * reanimate(socket, (nextsocket) => {
 *   // socket is reanimated
 *   handleSocket(socket);
 * });
 * ```
 */
function reanimate(
  socket: WebSocket,
  cb: (socket: WebSocket) => unknown,
  opts: ReanimateOpts = {},
): void {
  const { backoff = exponential, delay = 1000 } = opts;

  /** Set up the WebSocket for reanimation. */
  function rigSocket(socket: WebSocket, attempt = 0) {
    socket.addEventListener('close', reopen, { once: true });

    /** Whether the socket has been closed manually. */
    let closed = false;

    // Track when a WebSocket is manually closed, and stop the generator.
    const close = socket.close.bind(socket);
    socket.close = function (...args) {
      closed = true;
      close(...args);
    };

    function reopen() {
      if (!closed) {
        setTimeout(() => {
          socket = new WebSocket(socket.url);
          rigSocket(socket, attempt + 1);
          cb(socket);
        }, backoff(attempt, delay));
      }
    }
  }

  rigSocket(socket);
}

export { reanimate, type ReanimateOpts };
