/** Function that calculates the wait time between reconnection attempts. */
type Backoff = (attempt: number, delay: number, random?: typeof Math.random) => number;

/** Exponential backoff function. */
const exponential: Backoff = (attempt, delay, random = Math.random) => {
  return Math.floor(random() * Math.pow(2, attempt) * delay);
};

/** Fibonacci backoff function. */
const fibonacci: Backoff = (attempt, delay, random = Math.random) => {
  if (attempt <= 1) return delay;

  let prev = 0, current = 1;
  for (let index = 1; index < attempt; index++) {
    const next = prev + current;
    prev = current;
    current = next;
  }

  return Math.floor(random() * current * delay);
};

export { type Backoff, exponential, fibonacci };
