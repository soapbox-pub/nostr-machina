import { type Filter } from './deps.ts';

type FiltersFn = () => Filter[];

interface RelaySubOpts {
  uuid?: string;
  relays: WebSocket['url'][];
  filtersFn: FiltersFn;
  signal?: AbortSignal;
}

class RelaySub extends EventTarget {
  readonly uuid: string;
  readonly relays: string[];
  readonly filtersFn: FiltersFn;
  readonly signal?: AbortSignal;

  constructor(opts: RelaySubOpts) {
    super();
    this.uuid = opts.uuid ?? crypto.randomUUID();
    this.relays = opts.relays;
    this.filtersFn = opts.filtersFn;
    this.signal = opts.signal;
  }

  addEventListener<K extends keyof WebSocketEventMap>(
    type: K,
    // deno-lint-ignore no-explicit-any
    listener: (ev: WebSocketEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions,
  ): void;
  addEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
  ): void {
    super.addEventListener(type, listener, options);
  }

  removeEventListener<K extends keyof WebSocketEventMap>(
    type: K,
    // deno-lint-ignore no-explicit-any
    listener: (ev: WebSocketEventMap[K]) => any,
    options?: boolean | EventListenerOptions,
  ): void;
  removeEventListener(
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | EventListenerOptions,
  ): void {
    super.removeEventListener(type, listener, options);
  }
}

export { RelaySub, type RelaySubOpts };
