import { jsonSchema, relayUnknownSchema } from './schema.ts';

import { AsyncSocket } from './async-socket.ts';

/** Barebones relay client that deals directly with relay messages. */
class Relay {
  /** Relay's internal WebSocket connection. */
  socket: WebSocket;

  constructor(socket: WebSocket) {
    this.socket = socket;
  }

  /**
   * Get relay messages in an async iterable.
   * It goes on forever until the socket closes or the signal is aborted.
   *
   * @example
   * ```ts
   * for await (const msg of relay.stream(controller.signal)) {
   *   switch(msg[0]) {
   *     case 'EOSE':
   *       controller.abort();
   *       break;
   *     case 'EVENT':
   *       handleEvent(msg[2]);
   *       break;
   *   }
   * }
   * ```
   */
  async *stream(signal: AbortSignal): AsyncGenerator<[string, ...unknown[]]> {
    const messages = AsyncSocket.messages(this.socket, signal);

    for await (const { data } of messages) {
      const result = jsonSchema.pipe(relayUnknownSchema).safeParse(data);

      if (result.success) {
        yield result.data;
      }
    }
  }

  /** Send a Nostr message to the relay. If the relay isn't open yet, it will be opened. */
  send(msg: [string, ...unknown[]]): Promise<void> {
    const data = JSON.stringify(msg);
    return AsyncSocket.send(this.socket, data);
  }

  /**
   * Close the connection to the relay. This closes the socket.
   * Once closed, it cannot be reopened without creating a new `Relay` instance.
   */
  close(code?: number, reason?: string): Promise<void> {
    return AsyncSocket.close(this.socket, code, reason);
  }
}

export { Relay };
