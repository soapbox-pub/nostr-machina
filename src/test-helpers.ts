import { AsyncSocket } from './async-socket.ts';
import { sleep } from './async-utils.ts';

/** Open a WebSocket server on a random high port to send & receive messages during tests. */
function testServer(onMessage?: (socket: WebSocket, ev: MessageEvent<string>) => unknown) {
  let url = '';
  let lastSocket: WebSocket | undefined;

  const serverController = new AbortController();

  const opts: Deno.ServeOptions = {
    port: 0,
    signal: serverController.signal,
    onListen: ({ hostname, port }) => {
      url = `ws://${hostname}:${port}/`;
    },
  };

  const server = Deno.serve(opts, (req) => {
    const { socket, response } = Deno.upgradeWebSocket(req);

    lastSocket?.close();
    lastSocket = socket;

    if (onMessage) {
      socket.onmessage = (ev) => {
        onMessage(socket, ev);
      };
    }

    server.finished.then(() => socket.close());
    return response;
  });

  return {
    url,
    server,
    close: async () => {
      await sleep(50);
      serverController.abort();
      await server.finished;
    },
    get waitForOpen() {
      return (async () => {
        while (!lastSocket) {
          await sleep(10);
        }
        await AsyncSocket.waitForOpen(lastSocket);
      })();
    },
    disconnect: async () => {
      if (!lastSocket) throw new Error('No socket to disconnect');
      await AsyncSocket.close(lastSocket);
      lastSocket = undefined;
    },
  };
}

export { testServer };
