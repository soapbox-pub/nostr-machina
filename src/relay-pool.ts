import { AsyncSocket } from './async-socket.ts';
import { type Backoff, exponential } from './backoff.ts';
import { RelaySub, type RelaySubOpts } from './relay-sub.ts';
import { Semaphore } from './semaphore.ts';

interface RelayConfig {
  enabled?: boolean;
  limitation?: {
    max_subscriptions?: number;
  };
}

interface RelayConfigMap {
  [relay: WebSocket['url']]: RelayConfig;
}

interface PoolConfig {
  relays?: RelayConfigMap;
  backoff?: Backoff;
  delay?: number;
}

class RelayPool {
  #subs = new Map<WebSocket['url'], Set<RelaySub>>();
  #sockets = new Map<WebSocket['url'], WebSocket>();
  #semaphores = new Map<WebSocket['url'], Semaphore>();

  #config: PoolConfig;

  constructor(config: PoolConfig = {}) {
    this.#config = config;
    this.#syncSemaphores();
  }

  /** Configuration object for the pool. */
  get config(): PoolConfig {
    return this.#config;
  }

  set config(config: PoolConfig) {
    this.#config = config;
    this.#syncSemaphores();
  }

  /** Sync semaphores with open relays from the config. */
  #syncSemaphores(): void {
    for (const [relay, _socket] of this.#sockets) {
      this.#initSemaphore(relay);
    }
  }

  /** Set or get semaphore for a specific relay. */
  #initSemaphore(relay: WebSocket['url']): Semaphore {
    const semaphore = this.#semaphores.get(relay);
    if (semaphore) {
      return semaphore;
    } else {
      const { max_subscriptions = 20 } = this.config.relays?.[relay]?.limitation ?? {};
      const newSemaphore = new Semaphore(max_subscriptions);
      this.#semaphores.set(relay, newSemaphore);
      return newSemaphore;
    }
  }

  /** Distribute WebSocket events to RelaySub instances of that socket. */
  #handleSocketEvent = (e: Event): void => {
    const { url } = e.target as WebSocket;
    for (const sub of this.#subs.get(url) ?? []) {
      sub.dispatchEvent(cloneEvent(e));
    }
  };

  /** Add the socket and event listeners. Send `REQ`s for existing subs to the socket. */
  #addSocket(socket: WebSocket, attempt = 0): void {
    socket.onopen = this.#handleSocketEvent;
    socket.onclose = this.#handleSocketEvent;
    socket.onmessage = this.#handleSocketEvent;
    socket.onerror = this.#handleSocketEvent;
    this.#sockets.set(socket.url, socket);
    this.#initSemaphore(socket.url);
    this.#syncReqs(socket);

    /** When a socket closes, reopen it with exponential backoff as long as a sub exists for it. */
    const maybeReopen = () => {
      const { backoff = exponential, delay = 1000 } = this.config;
      if (this.#hasSubs(socket.url)) {
        setTimeout(() => {
          if (this.#hasSubs(socket.url)) {
            this.#addSocket(new WebSocket(socket.url), attempt + 1);
          } else {
            this.#sockets.delete(socket.url);
          }
        }, backoff(attempt, delay));
      } else {
        this.#sockets.delete(socket.url);
      }
    };

    socket.addEventListener('close', maybeReopen, { once: true });
  }

  /** Whether there are any subs for the relay. */
  #hasSubs(relay: WebSocket['url']): boolean {
    return (this.#subs.get(relay)?.size ?? 0) > 0;
  }

  /** Set up the relay in the store. */
  #initRelay(relay: WebSocket['url'], sub: RelaySub): void {
    if (!this.#sockets.has(relay)) {
      this.#addSocket(new WebSocket(relay));
    } else {
      const socket = this.#sockets.get(relay)!;
      this.#sendReq(socket, sub).catch(() => {});
    }
  }

  /** Add the sub to the store for the relay. */
  #addRelaySub(relay: WebSocket['url'], sub: RelaySub): void {
    if (!this.#subs.has(relay)) {
      this.#subs.set(relay, new Set<RelaySub>());
    }
    this.#subs.get(relay)!.add(sub);
  }

  /** Add the sub to the pool. */
  #addSub(sub: RelaySub): void {
    if (sub.signal?.aborted) return;

    for (const relay of sub.relays) {
      this.#addRelaySub(relay, sub);
      this.#initRelay(relay, sub);
    }

    sub.signal?.addEventListener('abort', () => {
      this.#removeSub(sub);
    });
  }

  /** Remove the sub from the pool. */
  #removeSub(sub: RelaySub): void {
    for (const relay of sub.relays) {
      this.#subs.get(relay)?.delete(sub);
      const socket = this.#sockets.get(relay);
      if (socket) {
        this.#sendClose(socket, sub).catch(() => {});
      }
    }
  }

  /** Send `REQ`s for existing subs to the socket. */
  #syncReqs(socket: WebSocket): void {
    for (const sub of this.#subs.get(socket.url) ?? []) {
      this.#sendReq(socket, sub).catch(() => {});
    }
  }

  /** Send a `REQ` for the sub to the socket. */
  async #sendReq(socket: WebSocket, sub: RelaySub): Promise<void> {
    await this.#initSemaphore(socket.url).acquire({ signal: sub.signal });
    const filters = sub.filtersFn();
    const msg = ['REQ', sub.uuid, ...filters] as const;
    return this.#send(socket, msg, sub.signal);
  }

  /** Send a `CLOSE` message for the sub to the socket. */
  #sendClose(socket: WebSocket, sub: RelaySub): Promise<void> {
    const msg = ['CLOSE', sub.uuid] as const;
    return this.#send(socket, msg);
  }

  /** Send a Nostr message to the relay. */
  #send(socket: WebSocket, msg: readonly [string, ...unknown[]], signal?: AbortSignal): Promise<void> {
    const data = JSON.stringify(msg);
    return AsyncSocket.send(socket, data, signal);
  }

  /** Subscribe to a filter on the chosen relays. */
  req(opts: RelaySubOpts): RelaySub {
    const sub = new RelaySub(opts);
    this.#addSub(sub);
    return sub;
  }
}

/** Clone an event, but replace the `target` property with the original target. */
function cloneEvent<T extends Event>(e: T): T {
  const Constructor = e.constructor as typeof Event;
  const clonedEvent = new Constructor(e.type, e) as T;

  return new Proxy(clonedEvent, {
    get(target, prop) {
      if (prop === 'target') {
        return e.target;
      }
      return target[prop as keyof T];
    },
  });
}

export { RelayPool };
