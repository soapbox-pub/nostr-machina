import { assertThrows } from './deps-test.ts';
import { nostrIdSchema } from './schema.ts';

Deno.test('nostrIdSchema', () => {
  nostrIdSchema.parse('79c2cae114ea28a981e7559b4fe7854a473521a8d22a66bbab9fa248eb820ff6');
  assertThrows(() => nostrIdSchema.parse('0123456789abcdef'));
  assertThrows(() => nostrIdSchema.parse('!!!'));
});
