import { assertEquals } from './deps-test.ts';
import { Machina } from './machina.ts';

Deno.test('push, iterate, & close', async () => {
  const results = [];
  const machina = new Machina<number>();

  machina.push(1);
  machina.push(2);
  setTimeout(() => machina.push(3), 100);

  for await (const msg of machina.stream()) {
    results.push(msg);

    if (results.length === 3) {
      machina.close();
    }
  }

  assertEquals(results, [1, 2, 3]);
});

Deno.test('close & reopen', async () => {
  const machina = new Machina<number>();

  machina.push(777);
  for await (const msg of machina.stream()) {
    assertEquals(msg, 777);
    machina.close();
  }

  machina.push(888);
  for await (const msg of machina.stream()) {
    assertEquals(msg, 888);
    machina.close();
  }
});
