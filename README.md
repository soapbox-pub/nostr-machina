# Nostr Machina

A library for working with Nostr WebSockets.

This library has been superseded by [NSpec](https://gitlab.com/soapbox-pub/NSpec).
