import { build, emptyDir } from 'https://deno.land/x/dnt@0.34.0/mod.ts';

await emptyDir('./npm');

await build({
  entryPoints: ['./mod.ts'],
  outDir: './npm',
  shims: {},
  package: {
    name: 'nostr-machina',
    version: Deno.args[0],
    description: 'A library for working with Nostr WebSockets.',
    license: 'MIT',
    repository: {
      type: 'git',
      url: 'git+https://gitlab.com/soapbox-pub/nostr-machina.git',
    },
    bugs: {
      url: 'https://gitlab.com/soapbox-pub/nostr-machina/-/issues',
    },
  },
  compilerOptions: {
    lib: ['esnext', 'dom'],
  },
  typeCheck: false,
  test: false,
  postBuild() {
    Deno.copyFileSync('LICENSE', 'npm/LICENSE');
    Deno.copyFileSync('README.md', 'npm/README.md');
  },
});
